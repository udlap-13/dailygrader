///<reference path="libs/jquery-2.1.4.min.js" />
function initAdd()
{
    //Alliases
    //PLACEHOLDER Id
    var id = localStorage.getItem("id");      //Processing to get numerical id
    //Initializing listeners
    $('#addButton').on('click',
        function () {
            //Data for submit
            var dataHolder =
            {
                ActId: id,
                Name: $('#inputNombre').val(),
                HIni: $('#inputHorarioPrincipio').val(),
                HFin: $('#inputHorarioFin').val(),
                Lugar: $('#inputLugar').val(),
                Stars: getScore(),
                Repetir: document.getElementById('repeat').checked ? 1 : 0,
                Rep: getRepeatDays(),
                Tipo: window.location.search.substring(1)
            }
            //String for data submission to server
            var dataString = 'id=' + dataHolder.ActId +
                '&name=' + dataHolder.Name +
                '&hIni=' + dataHolder.HIni +
                '&hFin=' + dataHolder.HFin +
                '&lugar=' + dataHolder.Lugar +
                '&stars=' + dataHolder.Stars +
                '&repetir=' + dataHolder.Repetir +
                '&rep=' + dataHolder.Rep +
				'&tipo='+dataHolder.Tipo;
            //Posting
            $.ajax(
                {
                    type: 'POST',
                    url: 'server/activityPost.php',
                    data: dataString,
                    success: function(answer){
                      if(answer==-1){
                        alert("Actividad inválida");
                      }
                    },
                    error: function(){
                        alert('Something went wrong, ERROR');
                        console.log(dataString);
                    }
                });
        });
}
//Function for reading repeats
function getRepeatDays()
{
    //String construct
    var repeatString = "";
    //Checking for weekdays
    var lunes = document.getElementById('lunes').checked ? 1 : 0;
    var martes = document.getElementById('martes').checked ? 1 : 0;
    var miercoles = document.getElementById('miercoles').checked ? 1 : 0;
    var jueves = document.getElementById('jueves').checked ? 1 : 0;
    var viernes = document.getElementById('viernes').checked ? 1 : 0;
    var sabado = document.getElementById('sabado').checked ? 1 : 0;
    var domingo = document.getElementById('domingo').checked ? 1 : 0;
    //Filling string
    repeatString = (lunes == 1 ? "Lu" : "") + (martes == 1 ? "Ma" : "") + (miercoles == 1 ? "Mi" : "") +
        (jueves == 1 ? "Ju" : "") + (viernes == 1 ? "Vi" : "") + (sabado == 1 ? "Sa" : "") +
        (domingo == 1 ? "Do" : "");
    return repeatString;
}
//Check radios for stars
function getScore()
{
    if (document.getElementById('star-5').checked) { return 5; }
    else if (document.getElementById('star-4').checked) { return 4; }
    else if (document.getElementById('star-3').checked) { return 3; }
    else if (document.getElementById('star-2').checked) { return 2; }
    else if (document.getElementById('star-1').checked) { return 1; }
    else { return 0 };
}
