function today(){
  var today = new Date();
  var year = today.getFullYear();
  var month = today.getMonth() + 1;
  var day = today.getDate();

  initDate = year + '-' + month + '-' + day + ' ' + '00:00';    //Hoy a las 00hrs
  endDate = year + '-' + month + '-' + day + ' ' + '23:59';   //Hoy a las 23:59hrs

  updateActivities();
}

function yesterday(){
  var yesterday = (function(){this.setDate(this.getDate()-1); return this}).call(new Date);
  var year = yesterday.getFullYear();
  var month = yesterday.getMonth() + 1;
  var day = yesterday.getDate();

  initDate = year + '-' + month + '-' + day + ' ' + '00:00';    //Ayer a las 00hrs
  endDate = year + '-' + month + '-' + day + ' ' + '23:59';   //Hoy a las 23:59hrs

  updateActivities();
}

function thisWeek(){
  lastWeek = (function getLastWeek(){
    var today = new Date();
    var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
    return lastWeek ;
  })();

  var month = lastWeek.getMonth() + 1;
  var day = lastWeek.getDate();
  var year = lastWeek.getFullYear();
  initDate = year + '-' + month + '-' + day + ' ' + '00:00';

  var today = new Date();
  year = today.getFullYear();
  month = today.getMonth() + 1;
  day = today.getDate();
  endDate = year + '-' + month + '-' + day + ' ' + '23:59';   //Hoy a las 23:59hrs

  updateActivities();
}

function thisMonth(){
  var today = new Date();
  year = today.getFullYear();
  month = today.getMonth() + 1;
  day = today.getDate();

  initDate = year + '-' + month + '-' + 1 + ' ' + '00:00';
  endDate = year + '-' + month + '-' + day + ' ' + '23:59';   //Hoy a las 23:59hrs

  updateActivities();
}

function custom(){
  var ini = $("#rango1").val();
  var end = $("#rango2").val();
  if(ini=="" || end=="")
    alert("Falta llenar un campo");
  else{
    initDate = ini + ' ' + '00:00';
    endDate = end + ' ' + '23:59';

    updateActivities();
  }
}
