///<reference path="libs/jquery-2.1.4.min.js" />

//Asegurandome que sean globales
var initDate;
var endDate;

function load() {
  //Id
  id = localStorage.getItem("id");
  if(id==null)
    window.location.replace("index.html")

  var today = new Date();
  var todayYear = today.getFullYear();
  var todayMonth = today.getMonth() + 1;
  var todayDay = today.getDate();

  initDate = todayYear + '-' + todayMonth + '-' + todayDay + ' ' + '00:00';    //Hoy a las 00hrs
  endDate = todayYear + '-' + todayMonth + '-' + todayDay + ' ' + '23:59';   //Hoy a las 24hrs


  updateActivities();
}

function updateActivities()
{
    /*
    Debo de darle el id del usuario (es global), con el login me responde con el id, con -1
    si es incorrecto.

    Para la info de rango de horario:
    Id, horario de inicio, horario de fin. Me dan un json.

    Cuando el cliente mande datos de fecha, convertir a formato mysql.

    Agus conversion de input de usuario a formato mysql. Se lo mando el server sacado del input y probablemente lo
    pueda leer. Avisarle a Agus si no funciona.

    Poner nuevos Hs para diferentes dias.

    Si no hay id, el main debe redirige al index (login). Usar replace para eso.
    */

    //id=<num>$hIni=<"YYYY-MM-DD HH:mm">$hFin=<"YYYY-MM-DD HH:mm">
    //Preparing data string
    var dataString = 'id=' + id +
        '&hIni=' + initDate +
        '&hFin=' + endDate;

    //The html dom container
    var target = $('#contentTable');            //Getting the table and other stuff
    //console.log(id);

    //Using JQuery to get all the stuff from the server and update it on the website.
    $.ajax({
        type: 'POST',                            //GET request
        url: 'server/activityRequest.php',    //Url of the server data
        data: dataString,
        success: function (answer) {
            var arr = $.parseJSON(answer);
            console.log(arr);
            //Emptying target table
            $("#contentTable tr").remove();
            //Metadata
            var incrementer = 1;        //This variable increases to generate the stars id
            var lastDate;                //Last day registered
            var currentTableId = '';     //Variable for dynamic table id generation
            var currentTableCounter = 0;    //Variable for dynamic table id generation
            //Alliases
            var mainPanel = $('#actContent').html("");
            //Preparing data for graphication
            var plotAca = 0;    //Horas academicas
            var plotOci = 0;    //Horas de oscio
            var plotDor = 0;    //Horas de dormir
            var plotNul = checkSqlHours(initDate, endDate, endDate);    //Horas de inactividad
            for (var i = 0; i < arr.loader.length; i++) {
                console.log(arr.loader[i]);
                //Preparing data for graphication
                var endActDate = arr.loader[i].HFin;
                var insertDate = arr.loader[i].HIni;    //The current date, must extract yyyy/mm/dd only later on
                var actType = arr.loader[i].Tipo;
                //Handling time allocation per activity type
                if (actType == 'a') { plotAca += checkSqlHours(insertDate, endActDate, endDate) }
                else if (actType == 'o') { plotOci += checkSqlHours(insertDate, endActDate, endDate) }
                else { plotDor += checkSqlHours(insertDate, endActDate, endDate) }

                //Preparing data for html insertion
                var insertName = arr.loader[i].Name;
                var insertLocation = arr.loader[i].Lugar;
                insertDate = insertDate.substring(0, 10);   //Processing to get only relevant data
                //Calculating
                if(insertDate.length == 10)                //Making sure that it is a valid format
                {
                    //Null last date case (beginning case)
                    if(lastDate == null)
                    {
                        var insertionString = '<h3>' + insertDate + '</h3>';        //Preparing string for html append
                        mainPanel.append(insertionString);                 //Appending to the main content panel
                        lastDate = insertDate;                //The last date is now replaced by current date
                        currentTableId = 'contentTable-' + currentTableCounter; //Generating table id name
                        currentTableCounter++;            //Increasisng table counter by 1 for more procedural generation
                        //Creating new table
                        //String for table insertion
                        var insertionString2 = '<table id="' + currentTableId + '"><col width="70%"></table>';
                        //Inserting new table
                        mainPanel.append(insertionString2);
                        //Setting the target to point to the new table
                        target = $('#' + currentTableId);
                        console.log(target);

                    }
                    //Checking if the valid date format is different, then this means we have a different date
                    else if(lastDate != insertDate)
                    {
                        var insertionString = '<h3>' + insertDate + '</h3>';        //Preparing string for html append
                        mainPanel.append(insertionString);                 //Appending to the main content panel
                        lastDate = insertDate;                //The last date is now replaced by current date
                        currentTableId = 'contentTable-' + currentTableCounter; //Generating table id name
                        currentTableCounter++;            //Increasisng table counter by 1 for more procedural generation
                        //Creating new table
                        //String for table insertion
                        var insertionString2 = '<table id="' + currentTableId + '"><col width="70%"></table>';
                        //Inserting new table
                        mainPanel.append(insertionString2);
                        //Setting the target to point to the new table
                        target = $('#' + currentTableId);
                    }
                }
                var entryString =
                    //Table column for name
                    "<tr> <td>" + insertName + "</td>" +
                    //Table column for location
                    "<td>" + insertLocation + "</td>" +
                    //Table column for stars
                    '<td><div class="stars">' +
               '<form action="">' +
                 '<input class="star star-5" id="star-5-' + incrementer + '" type="radio" name="star"/>' +
                 '<label class="star star-5" for="star-5-' + incrementer + '"></label>' +
                 '<input class="star star-4" id="star-4-' + incrementer + '" type="radio" name="star"/>' +
                 '<label class="star star-4" for="star-4-' + incrementer + '"></label>' +
                 '<input class="star star-3" id="star-3-' + incrementer + '" type="radio" name="star"/>' +
                 '<label class="star star-3" for="star-3-' + incrementer + '"></label>' +
                 '<input class="star star-2" id="star-2-' + incrementer + '" type="radio" name="star"/>' +
                 '<label class="star star-2" for="star-2-' + incrementer + '"></label>' +
                 '<input class="star star-1" id="star-1-' + incrementer + '" type="radio" name="star"/>' +
                 '<label class="star star-1" for="star-1-' + incrementer + '"></label>' +
               '</form>' +
             '</div>' +
                    //Table column for deletion. Makeshift class use.
                    '<td><a class="trash-' + incrementer + '" id="trash"></a></td> </tr>';
                target.append(entryString);
                //Get elements by ID. Then toggle stars as necessary and add listeners
                for (var s = 1; s <= 5; s++)        //Navigating all 5 stars
                {
                    //Getting the element we wish to toggle according to saved data
                    var currentStar = document.getElementById(('star-' + s + '-' + incrementer));
                    //Getting the saved data score
                    var score = arr.loader[i].Stars;
                    if (score == s) {
                        currentStar.checked = true;
                    }
                    //Adding listener to star:
                    //Preparing data for ajax update post
                    var actId = arr.loader[i].ActID;
                    var actScore = s;
                    var actString = '#star-' + s + '-' + incrementer;
                    //Enclosing to make sure it runs immediately
                    (function (address, inId, inScore) {
                        $(address).change(function () {
                            if ($(this).is(':checked')) {
                                //Preparing data for ajax update post

                                //Ajax work
                                $.ajax({
                                    type: 'POST',
                                    url: 'server/activityUpdate.php',
                                    data: 'actID=' + inId + '&grade=' + inScore,
                                    success: function (response) {
                                    },
                                    error: function () {
                                        alert('Could not update value, ERROR');
                                    }
                                });
                            }
                            else {
                                // Checkbox is not checked. Therefore do nothing
                            }
                        });
                    })(actString, actId, actScore);         //Passing parameters to make sure they save loop values
                }
                //Listener for the trashcan button. Enclosing to preserve loop values
                (function enclosedTrash(targetNum, inId)
                {
                    $('.trash-' + targetNum).click(function () {
                        //Deleting element
                        $.ajax({
                            type: 'POST',
                            url: 'server/activityDelete.php',
                            data: 'actID=' + inId,
                            success: function(answer)
                            {
                              updateActivities();
                            },
                            error: function()
                            {
                                alert('Could not delete, ERROR');
                            }
                        });
                    });
                })(incrementer, actId);
                //Increasing incrementer value
                incrementer++;
            }
            //Updating the null time counter
            plotNul -= plotAca + plotDor + plotOci;
            if (plotNul < 0) { plotNul = 0; }       //Preventing negative values
            //calcular tiempo muerto de acuerdo a cuantas horas existen en el rango menos las horas q ocupan las actividades
            graficar(plotAca, plotOci, plotDor, plotNul);
        },
        error: function () {
            alert('Something broke, ERROR');
        }

    });
    console.log(target);
 //Calculating hours between two SQL dates
    function checkSqlHours(startString, endString, endDate)
    {
        //Preparing data for Date creation
        //Date beginning stuff
        var yearI = 2015;
        var monthI = 1;
        var dayI = 1;
        var hourI = 0;
        var minuteI = 0;
        var secondI = 0;
        //Date ending stuff
        var yearE = 2015;
        var monthE = 1;
        var dayE = 1;
        var hourE= 0;
        var minuteE = 0;
        var secondE = 0;
        //Hour cropping variables
        var yearC = 2015;
        var monthC = 1;
        var dayC = 1;
        var hourC = 0;
        var minuteC = 0;
        var secondC = 0;
        //Reading beginning date strings
        if (startString.length >= 4) { yearI = Number(startString.substring(0, 4)); }
        if (startString.length >= 7) { monthI = Number(startString.substring(5, 7)); }
        if (startString.length >= 10) { dayI = Number(startString.substring(8, 10)); }
        if (startString.length >= 13) { hourI = Number(startString.substring(11, 13)); }
        if (startString.length >= 16) { minuteI = Number(startString.substring(14, 16)); }
        if (startString.length >= 19) { secondI = Number(startString.substring(17)); }
        //Reading end date strings
        if (endString.length >= 4) { yearE = Number(endString.substring(0, 4)); }
        if (endString.length >= 7) { monthE = Number(endString.substring(5, 7)); }
        if (endString.length >= 10) { dayE = Number(endString.substring(8, 10)); }
        if (endString.length >= 13) { hourE = Number(endString.substring(11, 13)); }
        if (endString.length >= 16) { minuteE = Number(endString.substring(14, 16)); }
        if (endString.length >= 19) { secondE = Number(endString.substring(17)); }
        //Reading crop end date strings, the one from the user defined search
        if (endDate.length >= 4) { yearC = Number(endDate.substring(0, 4)); }
        if (endDate.length >= 7) { monthC = Number(endDate.substring(5, 7)); }
        if (endDate.length >= 10) { dayC = Number(endDate.substring(8, 10)); }
        if (endDate.length >= 13) { hourC = Number(endDate.substring(11, 13)); }
        if (endDate.length >= 16) { minuteC = Number(endDate.substring(14, 16)); }
        if (endDate.length >= 19) { secondC = Number(endDate.substring(17)); }
        //Generating Date objects for comparisons
        var initDate = new Date(yearI, monthI, dayI, hourI, minuteI, secondI);      //Beginning date
        var endDate = new Date(yearE, monthE, dayE, hourE, minuteE, secondE);       //End date
        var cropDate = new Date(yearC, monthC, dayC, hourC, minuteC, secondC);      //The crop date object, limit
        //Getting time in miliseconds
        var initMiliSec = initDate.getTime();
        var endMiliSec = endDate.getTime();
        var cropMiliSec = cropDate.getTime();
        //Milisecond comparisons
        
        var limitDiference = cropMiliSec - endMiliSec;      //If negative then end date surpasses the limit.
        //and will be replaced by crop date, which is the limit defined by the user.
        if(limitDiference < 0) { endMiliSec = cropMiliSec; }    //If surpasses limit, then crop
        var miliDiference = endMiliSec - initMiliSec;       //Getting activity duration in miliseconds
        if (miliDiference > 0)  //Filtering to prevent negative values on miliDiference
        {
            var hourDiference = miliDiference / (1000 * 60 * 60);
            return hourDiference;
        }
        else {
            return 0;
        }
    }
}
