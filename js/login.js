$(document).ready(function(){
	$("#submit").click(function(){
		var name = $("#user").val();
		var password = $("#pass").val();

		// Returns successful data submission message when the entered information is stored in database.
		var dataString = 'user='+ name + '&pass='+ password;
		if(name==''||password==''){
			alert("Please Fill All Fields");
		}
		else{
			//AJAX code to submit form.
			$.ajax({
					type: "POST",
					url: "server/userRequest.php",
					data: dataString,
					cache: false,
					success: function(result){
							if(result==-1){     //if is not a valid user
								alert("Not a valid username or incorrect password")}   //
							else {     // saves the user id into global
							    localStorage.setItem("id", result);
							    window.location.replace("main.html");
							}
					}
			});
		}
		return false;
	});
});
