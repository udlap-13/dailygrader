$(document).ready(function(){
	$("#submit2").click(function(){
		var name = $("#user2").val();
		var password = $("#pass2").val();
		var passwordconfirm= $("#pass3").val();

		if(name==''||password==''||passwordconfirm==''){
			alert("Please fill all fields");
		}
		else{
			if(password!=passwordconfirm){
				alert("Passwords most be identical");
			}
			else{
				var dataString = 'user='+ name +'&pass=' + password;
				// AJAX Code To Submit Form.
				$.ajax({
						type: "POST",
						url: "server/userPost.php",
						data: dataString,
						cache: false,
						error: function(xhr, status, error) {
							alert("Something went wrong");
						  var err = eval("(" + xhr.responseText + ")");
						  console.log(err.Message);
						},
					  success: function (result) {
							if(result==-1){     //if is not a valid user
								alert("Error connectiong to server, please try again")}   //
							else if(result==-2)
								alert("Existing user")
							else {     // saves the user id into global
									localStorage.setItem("id", result);
									window.location.replace("main.html");
							}
					  }
				});
			}
		}
		return false;
	});
});
