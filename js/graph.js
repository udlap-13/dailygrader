function graficar(academ, ocio, dormir, muerto) {
	var chart = new CanvasJS.Chart("graph",
	{
    animationEnabled: true,
    data: [
		{
			type: "pie",
			toolTipContent: "{legendText}: <strong>{y}hrs</strong>",
			indexLabel: "{label}: {y}hrs",
			dataPoints: [
				{  y: muerto, legendText: "Tiempo muerto", label: "Tiempo muerto" }, 
				{  y: ocio, legendText: "Ocio", label: "Ocio" },
				{  y: dormir, legendText: "Dormir", label: "Dormir"},
				{  y: academ, legendText: "Academico", exploded: true, label: "Academico" }
			]
	}
	]
	});
	chart.render();
}
